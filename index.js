const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const path = require('path');
const app = express();



const { filesRouter } = require('./filesRouter.js');

app.use(express.json());

let accessLog = fs.createWriteStream(path.join(__dirname, 'access.log'),
  {
    flags: 'a'
  });

app.use(morgan('tiny', {
  stream: accessLog
}));

app.use('/api/files', filesRouter);

app.get('/', (req, res) => {
  res.send('hw1');
});

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();


//ERROR HANDLER
app.use(errorHandler)

function errorHandler(err, req, res, next) {
  console.error('err')
  res.status(500).send({ 'message': 'Server error' });
}
