const fs = require('fs');
const express = require('express');
const app = express();
const path = require('path');

const allowedExt = [
  '.log',
  '.txt',
  '.json',
  '.yaml',
  '.xml',
  '.js'
];

function createFile(req, res, next) {
  const { filename, content, password } = req.body;
  if (!filename || !allowedExt.includes(path.extname(filename))) {
    res.status(400).send({
      'message': 'Unsupported file extension'
    });
  } else {
    fs.writeFile(
      `files/${filename}`,
      content,
      {
        encoding: 'utf-8'
      },
      (err) => {
        if (err) {
          throw err;
        };
      }

    );
    if (password) {
      fs.access('password.json', fs.constants.F_OK, (err) => {
        if (err) {
          fs.writeFile(
            `password.json`,
            JSON.stringify({
              [filename]: password
            }),
            {
              encoding: 'utf-8'
            },
            (err) => {
              if (err) throw err;
            }
          );
        } else {
          fs.readFile('password.json', { encoding: 'utf-8' }, (err, data) => {
            if (err) throw err;
            if (!JSON.parse(data)) {
              return;
            }
            fs.writeFile(
              `password.json`,
              JSON.stringify({
                ...JSON.parse(data),
                [filename]: password
              }),
              {
                encoding: 'utf-8'
              },
              (err) => {
                if (err) throw err;
              }
            );
          });
        }
      });
    }
  }
  res.status(200).send({
    "message": "File created successfully"
  });
}

function getFiles(req, res, next) {
  fs.readdir('./files', function (err, data) {
    if (err) {
      throw err;
    } else {
      if (!data) {
        res.status(400).send({
          "message": "Array of files is empty"
        })
      }
      res.status(200).send({
        "message": "Success",
        "files": data
      });
    }
  });
}

const getFile = (req, res, next) => {
  const { filename } = req.params;
  const { password } = req.query;
  if (checkAccess(filename, password)) {
    fs.readFile(`./files/${filename}`,
      {
        encoding: 'utf-8'
      },
      function (err, data) {
        if (err) {
          res.status(400).send({
            'message': 'No file with provided name found.'
          });
        } else {
          fs.stat(`./files/${filename}`, (err, stat) => {
            if (err) {
              throw err;
            }
            res.status(200).send({
              "message": "Success",
              "filename": filename,
              "content": data,
              "extension": path.extname(filename).replace('.', ''),
              "uploadedDate": `${stat.birthtime.toISOString()}`
            });
          })
        }
      });
  } else {
    res.status(401).send({ 'message': 'Unauthorized' });
  }
}

const editFile = (req, res, next) => {
  const { filename, content, password } = req.body;
  if (checkAccess(filename, password)) {
    fs.access(`files/${filename}`, fs.constants.F_OK, (err) => {
      if (err) {
        res.status(400).send({
          'message': 'No file with provided name found.'
        });
      } else {
        fs.writeFile(
          `files/${filename}`,
          content,
          {
            encoding: 'utf-8'
          },
          (err) => {
            if (err) throw err;
          }
        );
        res.status(200).send({
          "message": "File edited successfully"
        });
      }
    });
  } else {
    res.status(401).send({ 'message': 'Unauthorized' });
  }
}

const deleteFile = (req, res, next) => {
  const { filename } = req.params;
  const { password } = req.query;
  if (checkAccess(filename, password)) {
    fs.access(
      `files/${filename}`,
      fs.constants.F_OK,
      (err) => {
        if (err) {
          res.status(400).send({
            'message': `No file with '${filename}' filename found`
          });
        } else {
          fs.unlink(
            `files/${filename}`,
            (err) => {
              if (err) throw err;
              res.status(200).send({
                'message': 'Deleted'
              })
            });
        }
      });
  } else {
    res.status(401).send({ 'message': 'Unauthorized' });
  }
}

function checkAccess(nameOfFile, attempt) {
  const data = fs.readFileSync(
    `password.json`,
    {
      encoding: 'utf-8'
    }
  );
  return JSON.parse(data)[nameOfFile] === attempt ? true : false;
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
